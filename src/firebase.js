import firebase from "firebase/compat/app";
import "firebase/compat/auth";
import { getAuth, onAuthStateChanged } from "firebase/auth";
import "firebase/compat/firestore";
import { getStorage } from "firebase/storage";
import { useEffect, useState } from "react";

const firebaseConfig = {
  apiKey: "AIzaSyCRNuQyE_LIGbrVtszYv4Ii2E1CcIKRiaw",
  authDomain: "ecommerce-bcfa4.firebaseapp.com",
  projectId: "ecommerce-bcfa4",
  storageBucket: "ecommerce-bcfa4.appspot.com",
  messagingSenderId: "519283217962",
  appId: "1:519283217962:web:68291b60cb8375905e1553",
};
export const app = firebase.initializeApp(firebaseConfig);
export const auth = firebase.auth();
export const googleAuthProvider = new firebase.auth.GoogleAuthProvider();
export const db = app.firestore();
export const storage = getStorage(app);
export const logout = () => {
  auth.signOut();
};

export const authorize = getAuth(app);

export const useAuth = () => {
  const [currentUser, setCurrentUser] = useState([]);
  useEffect(() => {
    const unsub = onAuthStateChanged(auth, (user) => {
      setCurrentUser(user);
    });
    return unsub;
  }, []);
  return currentUser;
};

export const fs = firebase;

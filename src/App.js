import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import "./App.scss";
import Cabinet from "./components/Cabinets";
import Contact from "./components/Contact";
import Graphics from "./components/Graphics";
import Header from "./components/Header";
import Home from "./components/Home";
import Keyboard from "./components/Keyboard";
import Monitors from "./components/Monitors";
import { positions, Provider } from "react-alert";
import AlertTemplate from "react-alert-template-basic";
import Productadd from "./components/Productadd";
import ProductView from "./components/ProductView";
import Signup from "./components/Signup";
import Login from "./components/Login";
import Cart from "./components/Cart";
import CheckoutCart from "./components/CheckoutCart";
import BuyProduct from "./components/BuyProduct";
import Order from "./components/Order";

const options = {
  timeout: 5000,
  position: positions.BOTTOM_CENTER,
};

function App() {
  return (
    <>
      <Provider template={AlertTemplate} {...options}>
        <Router>
          <Header />
          <Routes>
            <Route exact path="/" element={<Home />} />
            <Route exact path="/keyboard-mouse" element={<Keyboard />} />
            <Route exact path="/Graphics" element={<Graphics />} />
            <Route exact path="/Cabinets" element={<Cabinet />} />
            <Route exact path="/Monitors" element={<Monitors />} />
            <Route exact path="/contact" element={<Contact />} />
            <Route exact path="/productadd" element={<Productadd />} />
            <Route exact path="/Signup" element={<Signup />} />
            <Route exact path="/Login" element={<Login />} />
            <Route exact path="/cart" element={<Cart />} />
            <Route
              exact
              path=":category/product/:id/:randid"
              element={<ProductView />}
            />
            <Route exact path="/checkout/:randid" element={<CheckoutCart />} />
            <Route exact path="/buy/:pname/:pid" element={<BuyProduct />} />
            <Route exact path="/orders" element={<Order />} />
          </Routes>
        </Router>
      </Provider>
    </>
  );
}

export default App;

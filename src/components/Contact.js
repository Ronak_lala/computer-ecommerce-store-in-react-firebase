import { User, PhoneOutgoing, EnvelopeSimple } from "phosphor-react";
import "../App.scss";
import "../sass/index.scss";
import logo from "./assets/images/63487-programming-computer.gif";
import { useState } from "react";
import { uid } from "uid";
import { db } from "../firebase";
import { useAlert } from "react-alert";

const Contact = () => {
  const alert = useAlert();
  const userid = uid(16);
  const [msg, setMsg] = useState({
    username: "",
    email: "",
    phno: "",
    message: "",
  });
  const handleChange = (e) => {
    const { name, value } = e.target;
    setMsg((event) => {
      return {
        ...event,
        [name]: value,
      };
    });
  };

  const Submit = async (e) => {
    e.preventDefault();
    if (
      msg.username === "" ||
      msg.email === "" ||
      msg.phno === "" ||
      msg.message === ""
    ) {
      alert.error("Please Fill The Whole Form");
    } else {
      const data = {
        username: msg.username,
        phno: msg.phno,
        email: msg.email,
        message: msg.message,
        userid: userid,
      };
      await db.collection("contacts").doc(userid).set(data);
      alert.success("Message Successfully Sent");
      window.location.href = "/";
    }
  };
  return (
    <>
      <section className="container">
        <main>
          <div className="form">
            <div className="left">
              <img src={logo} alt="" />
            </div>
            <div className="rightBox">
              <h1>Contact Us with just a Click!</h1>
              <div>
                <User size={18} />
                <input
                  type="text"
                  placeholder="Enter Your Name"
                  name="username"
                  defaultValue={msg.username}
                  onChange={handleChange}
                />
              </div>
              <div>
                <PhoneOutgoing size={18} />
                <input
                  type="number"
                  placeholder="Enter Your Phone Number"
                  name="phno"
                  defaultValue={msg.phno}
                  onChange={handleChange}
                />
              </div>
              <div>
                <EnvelopeSimple size={18} />
                <input
                  type="email"
                  placeholder="Enter Your Email Address"
                  name="email"
                  defaultValue={msg.email}
                  onChange={handleChange}
                />
              </div>
              <div>
                <textarea
                  placeholder="Message goes Here"
                  name="message"
                  defaultValue={msg.message}
                  onChange={handleChange}
                />
              </div>
              <input
                type="submit"
                value="Contact"
                className="submit"
                onClick={Submit}
              />
            </div>
          </div>
        </main>
      </section>
    </>
  );
};

export default Contact;

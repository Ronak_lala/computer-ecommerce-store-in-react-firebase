import { EnvelopeSimple, Password } from "phosphor-react";
import "../App.scss";
import "../sass/index.scss";
import logo from "./assets/images/63487-programming-computer.gif";
import { useEffect, useState } from "react";
import { app } from "../firebase";
import { useAlert } from "react-alert";
import "firebase/auth";
import { useNavigate } from "react-router-dom";

const Login = () => {
  const navigate = useNavigate();
  const alert = useAlert();
  const [user, setUser] = useState({
    email: "",
  });
  const handleInputs = (e) => {
    const { name, value } = e.target;
    setUser((event) => {
      return {
        ...event,
        [name]: value,
      };
    });
  };

  const Submit = async () => {
    console.log(user);
    await app
      .auth()
      .signInWithEmailAndPassword(user.email, user.password)
      .then(() => {
        alert.success("User Logged In");
        navigate(-1);
      })
      .catch((error) => {
        const errorCode = error.code;
        const newcode = errorCode.slice(5);
        alert.error(newcode);
      });
  };

  useEffect(() => {
    document.title = "Login";
  });

  return (
    <>
      <section className="container">
        <main>
          <div className="form">
            <div className="left">
              <img src={logo} alt="" />
            </div>
            <div className="rightBox">
              <h1>Login @ Delzuis</h1>
              <div className="div">
                <EnvelopeSimple size={18} />
                <input
                  type="email"
                  placeholder="Enter Your Email Address"
                  name="email"
                  defaultValue={user.email}
                  onChange={handleInputs}
                />
              </div>
              <div className="div">
                <Password size={18} />
                <input
                  type="password"
                  placeholder="Enter Your Password"
                  name="password"
                  defaultValue={user.password}
                  onChange={handleInputs}
                />
              </div>
              <input
                type="submit"
                value="Login"
                className="submit"
                onClick={Submit}
              />
            </div>
          </div>
        </main>
      </section>
    </>
  );
};

export default Login;

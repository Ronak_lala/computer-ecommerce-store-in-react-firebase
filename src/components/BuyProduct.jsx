import { useEffect, useMemo, useState } from "react";
import { useParams } from "react-router-dom";
import "../App.scss";
import "../sass/index.scss";
import "../sass/style.scss";
import { db, useAuth } from "../firebase";
import { useAlert } from "react-alert";
import { useNavigate } from "react-router";

const BuyProduct = () => {
  const { pid, pname } = useParams();
  const alert = useAlert();
  const navigate = useNavigate();

  const currentUser = useAuth();

  const [msg, setMsg] = useState({
    username: "",
    phno: "",
    qty: "",
    email: "",
    address: "",
    pincode: "",
    price: "",
  });

  const [product, setProduct] = useState([]);
  const handleChange = (e) => {
    const { name, value } = e.target;
    setMsg((event) => {
      return {
        ...event,
        [name]: value,
      };
    });
  };

  useEffect(() => {
    db.collection("products")
      .doc(pid)
      .get()
      .then((doc) => {
        const proddata = doc.data();
        if (proddata !== undefined) {
          setProduct(proddata);
        } else {
          window.location.href = "/";
        }
      })
      .catch((err) => console.log(err));

    document.title = product.pname;
  }, []);

  const randid = useMemo(() => {
    return Math.floor(1000 + Math.random() * 9000);
  }, []);

  const Submit = async (e) => {
    e.preventDefault();
    if (
      msg.username === "" ||
      msg.phno === "" ||
      msg.address === "" ||
      msg.pincode === ""
    ) {
      alert.error("Please Fill the whole Form");
    } else {
      msg.email = currentUser.email;
      msg.price = msg.qty * product.price;
      msg.pimg = product.pimg;
      msg.pname = product.pname;
      msg.pid = product.pid;
      db.collection("orders")
        .doc(`${pid}` + currentUser.email + randid)
        .set(msg)
        .then((re) => {
          alert.success("Order Placed Successfully");
          navigate("/");
        })
        .catch((err) => console.log(err));
    }
  };

  useEffect(() => {
    document.title = product.pname;
  });

  return (
    <>
      <section className="container">
        <main>
          <div className="form">
            <div className="left">
              <img src={product.pimg} alt={product.pimg} />
              <center>
                <h3>{pname}</h3>
              </center>
            </div>
            <div className="rightBox">
              <h1>Place Order Now</h1>
              <div className="div">
                <input
                  type="text"
                  placeholder="Enter Your Name"
                  name="username"
                  defaultValue={msg.username}
                  onChange={handleChange}
                />
              </div>
              <div className="div">
                <input
                  type="number"
                  placeholder="Enter Your Phone Number"
                  name="phno"
                  defaultValue={msg.phno}
                  onChange={handleChange}
                />
              </div>
              <div className="div">
                <input
                  type="email"
                  placeholder="Enter Your Email Address"
                  name="email"
                  defaultValue={currentUser.email}
                  onChange={handleChange}
                />
              </div>
              <div className="div">
                <select name="qty" id="qty" onChange={handleChange}>
                  <option value="-------">---------</option>
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                  <option value="5">5</option>
                  <option value="6">6</option>
                  <option value="7">7</option>
                  <option value="8">8</option>
                  <option value="9">9</option>
                  <option value="10">10</option>
                </select>
              </div>
              <div className="div">
                <input
                  type="text"
                  placeholder="Enter Your Address"
                  name="address"
                  onChange={handleChange}
                />
              </div>
              <div className="div">
                <input
                  type="number"
                  placeholder="Enter Your Pincode"
                  name="pincode"
                  onChange={handleChange}
                />
              </div>
              <input
                type="submit"
                value="Place Order"
                className="submit"
                onClick={Submit}
              />
            </div>
          </div>
        </main>
      </section>
    </>
  );
};

export default BuyProduct;

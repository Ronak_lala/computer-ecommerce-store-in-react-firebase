import "../sass/header.scss";
import { List, X } from "phosphor-react";
import $ from "jquery";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router";
import { db, logout, useAuth } from "../firebase";
import { useAlert } from "react-alert";

const Header = () => {
  const alert = useAlert();
  const currentUser = useAuth();
  const navigate = useNavigate();
  const [count, setCount] = useState([]);

  const logoutuser = () => {
    logout();
    navigate("/login");
    alert.success("logged Out");
  };

  useEffect(() => {
    $("#menu-show").on("click", function () {
      $(".menu2").fadeIn();
    });

    $(".closemenu").on("click", function () {
      $(".menu2").fadeOut();
    });
    if (currentUser != null) {
      db.collection("cart")
        .where("useremail", "==", `${currentUser.email}`)
        .onSnapshot((snapshot) => {
          setCount(snapshot.docs.map((doc) => doc.data()));
        });
    }
  });

  return (
    <>
      <div className="header">
        <div className="cover-header">
          <div className="left">
            <a href="/">
              <h2>Delzuis</h2>
            </a>
          </div>
          <div className="menu">
            <ul>
              <a href="/keyboard-mouse">
                <li>Keyboards & Mouse</li>
              </a>
              <a href="/Graphics">
                <li>Graphics Card</li>
              </a>
              <a href="/Cabinets">
                <li>Cabinets</li>
              </a>
              <a href="/Monitors">
                <li>Monitors</li>
              </a>
              {currentUser === null ? (
                <>
                  <a href="/Signup">
                    <li>Signup</li>
                  </a>
                  <a href="/Login">
                    <li>Login</li>
                  </a>
                </>
              ) : (
                <>
                  {(currentUser.email === null) &
                  (currentUser.displayName !== null) ? (
                    <li>{currentUser.displayName}</li>
                  ) : (
                    <a href="/cart">
                      <li>Cart ({count.length})</li>
                    </a>
                  )}

                  <a href="/orders">
                    <li>Orders</li>
                  </a>

                  <li onClick={logoutuser}>Logout</li>
                </>
              )}
            </ul>
          </div>
          <div className="menu2" id="menuthis">
            <div className="closemenu">
              <X size={32} />
            </div>
            <ul>
              <a href="/keyboard-mouse">
                <li>Keyboards & Mouse</li>
              </a>
              <a href="/Graphics">
                <li>Graphics Card</li>
              </a>
              <a href="/Cabinets">
                <li>Cabinets</li>
              </a>
              <a href="/Monitors">
                <li>Monitors</li>
              </a>
              {currentUser == null ? (
                <>
                  <a href="/Signup">
                    <li>Signup</li>
                  </a>
                  <a href="/Login">
                    <li>Login</li>
                  </a>
                </>
              ) : (
                <>
                  {(currentUser.email === null) &
                  (currentUser.displayName !== null) ? (
                    <li>{currentUser.displayName}</li>
                  ) : (
                    <a href="/cart">
                      <li>Cart ({count.length})</li>
                    </a>
                  )}

                  <li onClick={logoutuser}>Logout</li>
                </>
              )}
            </ul>
          </div>
          <div className="icon" id="menu-show">
            <List size={32} />
          </div>
        </div>
      </div>
    </>
  );
};

export default Header;

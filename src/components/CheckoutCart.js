import { useEffect, useState, useMemo } from "react";
import { useAlert } from "react-alert";
import { useNavigate } from "react-router";
import { db, useAuth } from "../firebase";
import "../sass/style.scss";
import logo from "./assets/images/undraw_Add_to_cart_re_wrdo.png";
import { writeBatch } from "firebase/firestore";

const CheckoutCart = () => {
  const [cartData, setCartData] = useState([]);
  const currentUser = useAuth();
  const navigate = useNavigate();
  const alert = useAlert();
  const batch = writeBatch(db);
  const [loading, setLoading] = useState();

  const [msg, setMsg] = useState({
    username: "",
    phno: "",
    qty: "",
    email: "",
    address: "",
    pincode: "",
    price: "",
  });
  const handleChange = (e) => {
    const { name, value } = e.target;
    setMsg((event) => {
      return {
        ...event,
        [name]: value,
      };
    });
  };

  const randid = useMemo(() => {
    return Math.floor(1000 + Math.random() * 9000);
  }, []);

  const getData = () => {
    db.collection("cart")
      .where("useremail", "==", `${currentUser.email}`)
      .get()
      .then((snapshot) => {
        const logs = [];
        snapshot.forEach((doc) => {
          if (doc.data()) {
            const data = {
              pid: doc.data().pid,
              pimg: doc.data().pimg,
              pname: doc.data().pname,
              qty: doc.data().qty,
              price: doc.data().price,
            };
            logs.push(data);
          }
        });
        setCartData(logs);
      });
  };

  useEffect(() => {
    getData();
  });

  const Submit = async (e) => {
    e.preventDefault();
    if (
      msg.username === "" ||
      msg.phno === "" ||
      msg.address === "" ||
      msg.pincode === ""
    ) {
      alert.error("Please Fill the whole Form");
    } else {
      msg.email = currentUser.email;
      cartData.forEach(async (doc) => {
        msg.qty = doc.qty;
        msg.price = msg.qty * doc.price;
        msg.pimg = doc.pimg;
        msg.pname = doc.pname;
        msg.pid = doc.pid;
        batch.set(
          db
            .collection("orders")
            .doc(doc.pid + `${currentUser.email}` + randid),
          msg
        );
        batch.delete(
          db.collection("cart").doc(doc.pid + `${currentUser.email}`),
          msg
        );
        setLoading(true);
      });
      return batch
        .commit()
        .then((re) => {
          alert.success("Order Placed Successfully");
          setLoading(false);
          navigate("/");
        })
        .catch((err) => console.log(err));
    }
  };

  function total() {
    let x = 0;
    cartData.map((cartdata) => {
      x += cartdata.price * cartdata.qty;
    });
    return x;
  }

  return (
    <>
      <section className="container">
        <main>
          <div className="prod_block">
            {cartData.map(({ pid, pimg, pname, price, qty }) => {
              const totalprice = qty * price;
              return (
                <>
                  <div className="block">
                    <span>Product Name</span> : <span>{pname}</span> x{" "}
                    <span>{qty}</span> -{" "}
                    <span>Rs {totalprice.toLocaleString()}</span>
                  </div>
                </>
              );
            })}
            <div className="totalbx">
              <h3>Total </h3>
              <h4>Rs {total().toLocaleString()}</h4>
            </div>
          </div>

          <div className="form">
            <div className="left">
              <img src={logo} alt={logo} />
            </div>

            <div className="rightBox">
              {loading === true ? (
                <>
                  <center>
                    <span style={{ color: "#000" }}>Placing Order.....</span>
                  </center>
                </>
              ) : (
                <></>
              )}
              <h1>Place Order Now</h1>
              <div className="div">
                <input
                  type="text"
                  placeholder="Enter Your Name"
                  name="username"
                  defaultValue={msg.username}
                  onChange={handleChange}
                />
              </div>
              <div className="div">
                <input
                  type="number"
                  placeholder="Enter Your Phone Number"
                  name="phno"
                  defaultValue={msg.phno}
                  onChange={handleChange}
                />
              </div>
              <div className="div">
                <input
                  type="email"
                  placeholder="Enter Your Email Address"
                  name="email"
                  defaultValue={currentUser.email}
                  onChange={handleChange}
                />
              </div>
              <div className="div">
                <input
                  type="text"
                  placeholder="Enter Your Address"
                  name="address"
                  onChange={handleChange}
                />
              </div>
              <div className="div">
                <input
                  type="number"
                  placeholder="Enter Your Pincode"
                  name="pincode"
                  onChange={handleChange}
                />
              </div>
              <input
                type="submit"
                value="Place Order"
                className="submit"
                onClick={Submit}
              />
            </div>
          </div>
        </main>
      </section>
    </>
  );
};

export default CheckoutCart;

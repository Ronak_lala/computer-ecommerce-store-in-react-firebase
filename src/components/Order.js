import { useEffect, useState } from "react";
import { useAlert } from "react-alert";
import { db, useAuth } from "../firebase";
import "../sass/style.scss";

const Order = () => {
  const [cartData, setCartData] = useState([]);
  const currentUser = useAuth();
  const [loading, setLoading] = useState();

  const alert = useAlert();

  const data = async () => {
    await db
      .collection("orders")
      .where("email", "==", `${currentUser.email}`)
      .get()
      .then((snapshot) => {
        const logs = [];
        snapshot.forEach((doc) => {
          if (doc.data()) {
            const data = {
              pid: doc.data().pid,
              pimg: doc.data().pimg,
              pname: doc.data().pname,
              qty: doc.data().qty,
              price: doc.data().price,
            };
            logs.push(data);
          }
        });
        setCartData(logs);
      });
  };

  const deleteProd = async (pid) => {
    setLoading(true);
    await db
      .collection("orders")
      .where("pid", "==", `${pid}`)
      .get()
      .then(function (querySnapshot) {
        querySnapshot.forEach(function (doc) {
          doc.ref.delete().then(() => {
            setLoading(false);
            alert.success("Order Cancelled");
          });
        });
      })
      .catch((err) => console.log(err));
  };

  useEffect(() => {
    data();
    document.title = "Orders";
  }, [cartData]);
  return (
    <>
      <section className="container">
        <main>
          <h1>Orders</h1>
          {cartData.length === 0 ? (
            <>
              <center>
                <h2>No Current Orders</h2>
              </center>
            </>
          ) : (
            <>
              {loading === true ? (
                <>
                  <center>
                    <span style={{ color: "#000" }}>Cancelling Order</span>
                  </center>
                </>
              ) : (
                <></>
              )}
              <table className="table">
                <thead>
                  <tr>
                    <th>Product Image</th>
                    <th>Product Name</th>
                    <th>Quantity</th>
                    <th>Total</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  {cartData.map(({ pid, pimg, pname, price, qty }) => {
                    const totalprice = qty * price;
                    return (
                      <>
                        <tr key={pid} id={pid}>
                          <td>
                            <img src={pimg} alt={pimg} />
                          </td>
                          <td>{pname}</td>
                          <td>{qty}</td>
                          <td>Rs {totalprice.toLocaleString()}</td>
                          <td>
                            <button id={pid} onClick={() => deleteProd(pid)}>
                              Cancel
                            </button>
                          </td>
                        </tr>
                      </>
                    );
                  })}
                </tbody>
              </table>
            </>
          )}
        </main>
      </section>
    </>
  );
};

export default Order;

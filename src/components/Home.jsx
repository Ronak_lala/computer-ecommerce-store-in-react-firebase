import { Swiper, SwiperSlide } from "swiper/react";

import "swiper/css";
import "swiper/css/navigation";

import { Navigation } from "swiper";

import "../sass/style.scss";
import { Link } from "react-router-dom";
import ProductCard from "./ProductCard";
import { useEffect, useState } from "react";
import { db } from "../firebase";

const Home = () => {
  const [product, setProduct] = useState([]);
  const data = async () => {
    await db
      .collection("products")
      .limit(12)
      .orderBy("pid", "asc")
      .get()
      .then((querySnapshot) => {
        querySnapshot.forEach((element) => {
          var data = element.data();
          setProduct((arr) => [...arr, data]);
        });
      });
  };

  useEffect(() => {
    data();
    document.title = "Home";
  }, []);
  return (
    <>
      <section className="container">
        <main>
          <br />
          <Swiper navigation={true} modules={[Navigation]} className="mySwiper">
            <SwiperSlide>
              <div className="sliderbox">
                <h1>
                  A Pc you might love so much and build your own happiness with
                  just clicks
                </h1>
                <Link to="/Graphics">
                  <button>Buy Graphic Cards Now</button>
                </Link>
              </div>
            </SwiperSlide>
            <SwiperSlide>
              <div className="sliderbox">
                <h1>
                  A big storage block with gaming colors in it, makes you feel
                  quite happy about your console.
                </h1>
                <Link to="/Cabinets">
                  <button>Buy Cabinets Now</button>
                </Link>
              </div>
            </SwiperSlide>
            <SwiperSlide>
              <div className="sliderbox">
                <h1>
                  Display that makes you feel energized about your games and
                  work will be best from Delzius.
                </h1>
                <Link to="/Monitors">
                  <button>Buy Monitors Now</button>
                </Link>
              </div>
            </SwiperSlide>
          </Swiper>

          <center>
            <h2>Featuring Some Products</h2>
            <div className="products">
              <center>
                <ul>
                  {product.map((products, i) => (
                    <ProductCard
                      img={products.pimg}
                      name={products.pname}
                      price={products.price}
                      pid={products.pid}
                      cat={products.prod_cat}
                      i={i}
                    />
                  ))}
                </ul>
              </center>
            </div>
          </center>
        </main>
      </section>
    </>
  );
};

export default Home;

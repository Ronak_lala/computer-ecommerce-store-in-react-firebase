import { useMemo } from "react";
import { Link } from "react-router-dom";
import { useAuth } from "../firebase";

const ProductCard = (props) => {
  const currentUser = useAuth();
  const randid = useMemo(() => {
    return Math.floor(1000 + Math.random() * 9000);
  }, []);

  return (
    <>
      <li key={props.pid} id={props.pid}>
        <img src={props.img} alt={props.name} />
        <div className="desc">
          <span className="pname">{props.name}</span>
          <span className="price">Rs. {props.price}/-</span>
          <div className="btns">
            {currentUser == null ? (
              <Link to="/Login">
                <button className="view">Login To View</button>
              </Link>
            ) : (
              <>
                <a href={`/${props.cat}/product/${props.pid}/${randid}`}>
                  <button className="view">View</button>
                </a>
              </>
            )}
          </div>
        </div>
      </li>
    </>
  );
};

export default ProductCard;

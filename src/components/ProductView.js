import { useEffect, useState } from "react";
import { useAlert } from "react-alert";
import { useParams, useNavigate } from "react-router-dom";
import { db, useAuth } from "../firebase";
import "../sass/main.scss";
import "../sass/product.scss";
import ProductCard from "./ProductCard";

const ProductView = () => {
  const alert = useAlert();
  const currentUser = useAuth();
  const { id, category } = useParams();
  const [cart, setCart] = useState([]);
  const [loading, setLoading] = useState();

  const [product, setProduct] = useState([]);

  const [similarproduct, setSimilarProduct] = useState([]);
  const navigate = useNavigate();

  const data = async () => {
    let cat = "";
    if (category === "Graphics") {
      cat = "Graphics Card";
    } else {
      cat = category;
    }
    await db
      .collection("products")
      .where("prod_cat", "==", cat)
      .where("pid", "!=", id)
      .limit(3)
      .get()
      .then((querySnapshot) => {
        querySnapshot.forEach((element) => {
          var data = element.data();
          setSimilarProduct((arr) => [...arr, data]);
        });
      });
  };

  const handleCart = async () => {
    setLoading(true);
    let cartData = {
      pid: product.pid,
      pname: product.pname,
      pimg: product.pimg,
      qty: 1,
      prod_cat: product.prod_cat,
      useremail: currentUser.email,
      price: product.price,
    };
    await db
      .collection("cart")
      .doc(product.pid + `${currentUser.email}`)
      .set(cartData)
      .then(() => {
        setLoading(false);
        alert.success("Product Added in Cart");
      })
      .catch((err) => console.log(err));
  };

  const proddata = async () => {
    await db
      .collection("products")
      .doc(id)
      .get()
      .then((doc) => {
        const newData = doc.data();
        if (newData !== undefined) {
          setProduct(newData);
        } else {
          setInterval(function () {
            navigate(-1);
          }, 2000);
        }
      });
  };

  useEffect(() => {
    proddata();
    data();
  }, []);

  db.collection("cart")
    .doc(id + `${currentUser.email}`)
    .get()
    .then((doc) => setCart(doc.data()));

  document.title = product.pname;

  return (
    <>
      <section className="container">
        <center>
          {loading === true ? (
            <>
              <center>
                <span style={{ color: "#000" }}>Adding to Cart....</span>
              </center>
            </>
          ) : (
            <></>
          )}
        </center>
        {product.length === 0 ? (
          <main>
            <center></center>
          </main>
        ) : (
          <main>
            <section className="product">
              <div className="left">
                <img src={product.pimg} alt={product.pimg} />
              </div>
              <div className="info">
                <h3>{product.pname}</h3>
                <span>Rs {product.price}</span>
                <h3>{product.prod_cat}</h3>
                <p>{product.prod_desc}</p>
                <div>
                  <a href={"/Buy/" + `${product.pname}/` + `${product.pid}`}>
                    <button>Buy Now</button>
                  </a>
                  {cart === undefined ? (
                    <button onClick={handleCart}>Add To Cart</button>
                  ) : (
                    <a href="/cart">
                      <button>View in Cart</button>
                    </a>
                  )}
                </div>
              </div>
            </section>

            <center>
              <h1>Similar Products</h1>
              <div className="products">
                <ul>
                  {similarproduct.map((sproducts, i) => (
                    <ProductCard
                      key={i}
                      img={sproducts.pimg}
                      name={sproducts.pname}
                      price={sproducts.price}
                      pid={sproducts.pid}
                      cat={sproducts.prod_cat}
                      i={i}
                    />
                  ))}
                </ul>
              </div>
            </center>
          </main>
        )}
      </section>
    </>
  );
};

export default ProductView;

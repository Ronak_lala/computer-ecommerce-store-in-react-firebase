import "../App.scss";
import ProductCard from "./ProductCard";
import { db } from "../firebase";
import { useState, useEffect } from "react";

const Cabinet = () => {
  const [product, setProduct] = useState([]);
  const data = async () => {
    await db
      .collection("products")
      .where("prod_cat", "==", "Cabinets")
      .get()
      .then((querySnapshot) => {
        querySnapshot.forEach((element) => {
          var data = element.data();
          setProduct((arr) => [...arr, data]);
        });
      });
  };

  /*
  HASSU KA SEARCH QUERY
  let datasearch=services.filter(item=>{
        return Object.keys(item).some(key=>item[key].toString().toLowerCase().includes(search.toString().toLowerCase()))
        })
    */

  useEffect(() => {
    data();
    document.title = "Cabinets";
  }, []);
  return (
    <>
      <br />
      <section className="container">
        <main>
          <section className="banner">
            <h1>Cabinets</h1>
          </section>
          {product.length === 0 ? (
            <center>
              <h2>No Products Found</h2>
            </center>
          ) : (
            <div className="products">
              <center>
                <h2>Total Cabinets :- {product.length}</h2>
                <ul>
                  {product.map((products, i) => (
                    <ProductCard
                      key={i}
                      img={products.pimg}
                      name={products.pname}
                      price={products.price}
                      cat={products.prod_cat}
                      pid={products.pid}
                      i={i}
                    />
                  ))}
                </ul>
              </center>
            </div>
          )}
        </main>
      </section>
    </>
  );
};

export default Cabinet;

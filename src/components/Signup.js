import { User, PhoneOutgoing, EnvelopeSimple, Password } from "phosphor-react";
import "../App.scss";
import "../sass/index.scss";
import logo from "./assets/images/63487-programming-computer.gif";
import { useEffect, useState } from "react";
import { app, authorize } from "../firebase";
import { useAlert } from "react-alert";
import "firebase/auth";
import { useNavigate } from "react-router-dom";
import { GoogleAuthProvider, signInWithPopup } from "firebase/auth";
import GoogleButton from "react-google-button";

const Signup = () => {
  const navigate = useNavigate();
  const alert = useAlert();
  const [user, setUser] = useState({
    username: "",
    phno: "",
    email: "",
    password: "",
    cpassword: "",
  });
  const handleInputs = (e) => {
    const { name, value } = e.target;
    setUser((event) => {
      return {
        ...event,
        [name]: value,
      };
    });
  };

  const Submit = async () => {
    if (user.password !== user.cpassword) {
      alert.error("Passwords do not Mathc");
    } else if (user.password.length < 6) {
      alert.error("Password Should be atleast 6 characters long");
    } else {
      await app
        .auth()
        .createUserWithEmailAndPassword(user.email, user.password)
        .then(() => {
          alert.success("User Registered");
          navigate(-1);
        })
        .catch((error) => {
          const errorCode = error.code;
          const newcode = errorCode.slice(5);
          console.log(newcode);
          alert.error(newcode);
        });
    }
  };
  const SignInWithGoogle = () => {
    const provider = new GoogleAuthProvider();
    signInWithPopup(authorize, provider)
      .then((re) => {
        navigate(-1);
        alert.success("Logged In Successfully");
      })
      .catch((error) => console.log(error));
  };

  useEffect(() => {
    document.title = "Sign-Up";
  });

  return (
    <>
      <section className="container">
        <main>
          <div className="form">
            <div className="left">
              <img src={logo} alt="" />
            </div>
            <div className="rightBox">
              <h1>Signup @ Delzuis</h1>
              <div className="div">
                <User size={18} />
                <input
                  type="text"
                  placeholder="Enter Your Name"
                  name="username"
                  defaultValue={user.username}
                  onChange={handleInputs}
                />
              </div>
              <div className="div">
                <PhoneOutgoing size={18} />
                <input
                  type="number"
                  placeholder="Enter Your Phone Number"
                  name="phno"
                  defaultValue={user.phno}
                  onChange={handleInputs}
                />
              </div>
              <div className="div">
                <EnvelopeSimple size={18} />
                <input
                  type="email"
                  placeholder="Enter Your Email Address"
                  name="email"
                  defaultValue={user.email}
                  onChange={handleInputs}
                />
              </div>
              <div className="div">
                <Password size={18} />
                <input
                  type="password"
                  placeholder="Enter Your Password"
                  name="password"
                  defaultValue={user.password}
                  onChange={handleInputs}
                />
              </div>
              <div className="div">
                <Password size={18} />
                <input
                  type="password"
                  placeholder="Confirm Your Password"
                  name="cpassword"
                  defaultValue={user.cpassword}
                  onChange={handleInputs}
                />
              </div>
              <input
                type="submit"
                value="Sign Up"
                className="submit"
                onClick={Submit}
              />
              <GoogleButton
                onClick={SignInWithGoogle}
                style={{ width: "250px" }}
              />
            </div>
          </div>
        </main>
      </section>
    </>
  );
};

export default Signup;

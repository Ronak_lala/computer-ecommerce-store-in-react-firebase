import { User, PhoneOutgoing, EnvelopeSimple } from "phosphor-react";
import "../App.scss";
import "../sass/index.scss";
import logo2 from "./assets/images/undraw_Investment_re_rpk5.png";
import { useEffect, useState } from "react";
import { uid } from "uid";
import { useAlert } from "react-alert";
import { storage, db } from "../firebase";
import { ref } from "@firebase/storage";
import { getDownloadURL, uploadBytesResumable } from "firebase/storage";
import $ from "jquery";

const Productadd = () => {
  useEffect(() => {
    document.title = "Add Product";
  });

  const [progress, setProgress] = useState(0);

  const alert = useAlert();
  const userid = uid(16);
  const [msg, setMsg] = useState({
    pname: "",
    price: "",
    prod_cat: "",
    prod_desc: "",
    pimt: "",
  });

  const handleImg = (e) => {
    const file = e.target.files[0];
    uploadImg(file);
    $(".h5").show();
  };

  const uploadImg = (file) => {
    if (!file) {
      alert.error("Empty Image");
    } else {
      const storageRef = ref(storage, "/product_images/" + file.name);
      const upload = uploadBytesResumable(storageRef, file);
      upload.on("state_changed", (snapshot) => {
        const prog = Math.round(
          (snapshot.bytesTransferred / snapshot.totalBytes) * 100
        );
        setProgress(prog);
        getDownloadURL(upload.snapshot.ref).then((url) => {
          msg.pimg = url;
        });
      });
    }
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setMsg((event) => {
      return {
        ...event,
        [name]: value,
      };
    });
  };
  const Submit = async (e) => {
    e.preventDefault();
    if (
      msg.pname === "" ||
      msg.price === "" ||
      msg.prod_cat === "" ||
      msg.prod_desc === "" ||
      msg.pimg === ""
    ) {
      alert.error("Please Fill The Whole Form");
    } else {
      const data = {
        pname: msg.pname,
        price: msg.price,
        prod_cat: msg.prod_cat,
        prod_desc: msg.prod_desc,
        pimg: msg.pimg,
        pid: userid,
        cart: "false",
      };
      console.log(data);

      await db
        .collection("products")
        .doc(userid)
        .set(data)
        .then(() => {
          alert.success("Done");
          setTimeout(function () {
            window.location.reload(1);
          }, 1000);
        })
        .catch((err) => {
          alert.error(err);
        });
    }
  };
  return (
    <>
      <section className="container">
        <main>
          <div className="form">
            <div className="left">
              <img src={logo2} alt={logo2} />
            </div>
            <div className="rightBox">
              <h1>Add Products</h1>
              <div className="div">
                <User size={18} />
                <input
                  type="text"
                  placeholder="Enter Product Name"
                  name="pname"
                  defaultValue={msg.pname}
                  onChange={handleChange}
                />
              </div>
              <div className="div">
                <PhoneOutgoing size={18} />
                <input
                  type="number"
                  placeholder="Enter Price"
                  name="price"
                  defaultValue={msg.price}
                  onChange={handleChange}
                />
              </div>
              <div className="div">
                <EnvelopeSimple size={18} />
                <select
                  name="prod_cat"
                  onChange={handleChange}
                  defaultValue={msg.prod_cat}
                  id="prod_cat"
                >
                  <option value="">----------------</option>
                  <option value="keyboard-mouse">Keyboard / Mouse</option>
                  <option value="Graphics Card">Graphics Card</option>
                  <option value="Monitors">Monitors</option>
                  <option value="Cabinets">Cabinets</option>
                </select>
              </div>
              <div className="div">
                <textarea
                  placeholder="Product Description"
                  name="prod_desc"
                  defaultValue={msg.prod_desc}
                  onChange={handleChange}
                />
              </div>
              <div className="div">
                <PhoneOutgoing size={18} />
                <input
                  type="file"
                  placeholder="Enter Product Image"
                  name="pimg"
                  defaultValue={msg.pimg}
                  onChange={handleImg}
                />
              </div>
              <span className="h5">Uploaded {progress} %</span>
              <input
                type="submit"
                value="Add Product"
                className="submit"
                id="submit"
                onClick={Submit}
              />
            </div>
          </div>
        </main>
      </section>
    </>
  );
};

export default Productadd;

import { useEffect, useMemo, useState } from "react";
import { db, useAuth } from "../firebase";

import "../sass/style.scss";

const Cart = () => {
  const [cartData, setCartData] = useState([]);
  const currentUser = useAuth();
  const [loading, setLoading] = useState();

  const getData = () => {
    db.collection("cart")
      .where("useremail", "==", `${currentUser.email}`)
      .get()
      .then((snapshot) => {
        const logs = [];
        snapshot.forEach((doc) => {
          if (doc.data()) {
            const data = {
              pid: doc.data().pid,
              pimg: doc.data().pimg,
              pname: doc.data().pname,
              qty: doc.data().qty,
              price: doc.data().price,
            };
            logs.push(data);
          }
        });
        setCartData(logs);
      });
  };

  useEffect(() => {
    getData();
    document.title = "Cart";
  });

  const increase = async (pid, qty) => {
    setLoading(true);
    try {
      await db
        .collection("cart")
        .doc(`${pid}` + `${currentUser.email}`)
        .update("qty", qty + 1)
        .then(() => {
          setLoading(false);
          window.location.reload();
        });
    } catch (err) {
      console.log(err);
    }
  };

  const decrease = async (pid, qty) => {
    setLoading(true);
    try {
      await db
        .collection("cart")
        .doc(`${pid}` + `${currentUser.email}`)
        .update("qty", qty - 1)
        .then(() => {
          setLoading(false);
          window.location.reload();
        });
    } catch (err) {
      console.log(err);
    }
  };

  const deleteProd = (pid) => {
    setLoading(true);
    db.collection("cart")
      .doc(pid + `${currentUser.email}`)
      .delete()
      .then(() => {
        setLoading(false);
        window.location.reload();
      })
      .catch((err) => console.log(err));
  };

  function total() {
    let x = 0;
    cartData.map((cartdata) => {
      x += cartdata.price * cartdata.qty;
    });
    return x;
  }

  const randomId = useMemo(() => {
    return Math.floor(100000000 + Math.random() * 90000000);
  }, []);

  return (
    <>
      <section className="container">
        <main>
          <h1>Cart</h1>
          {loading === true ? (
            <>
              <center>
                <span style={{ color: "#000" }}>Updating Cart.....</span>
              </center>
            </>
          ) : (
            <></>
          )}
          <table className="table">
            <thead>
              <tr>
                <th>Product Image</th>
                <th>Product Name</th>
                <th>Quantity</th>
                <th>Price</th>
                <th>Total</th>
                <th>Remove</th>
              </tr>
            </thead>
            <tbody>
              {cartData.map(({ pid, pimg, pname, price, qty }) => {
                const totalprice = qty * price;
                return (
                  <>
                    <tr key={pid} id={pid}>
                      <td>
                        <img src={pimg} alt={pimg} />
                      </td>
                      <td>{pname}</td>
                      <td>
                        <button onClick={() => decrease(pid, qty)}>-</button>{" "}
                        {qty}{" "}
                        <button onClick={() => increase(pid, qty)}>+</button>
                      </td>
                      <td>Rs {price.toLocaleString()}</td>
                      <td>Rs {totalprice.toLocaleString()}</td>
                      <td>
                        <button
                          className="delete"
                          onClick={() => deleteProd(pid)}
                        >
                          Delete
                        </button>
                      </td>
                    </tr>
                  </>
                );
              })}
            </tbody>
          </table>
          <div className="totalbx">
            <h3>Sub - Total </h3>
            <h4>Rs {total().toLocaleString()}</h4>
            <a href={"/checkout/" + `${randomId}`}>
              <button className="delete">Proceed to Checkout</button>
            </a>
          </div>
        </main>
      </section>
    </>
  );
};

export default Cart;
